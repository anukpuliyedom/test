// These are frontend packages that don't actually get used in mobile
declare module "ng-inline-svg";
declare module "ngx-clipboard";
declare const browser;
declare const Buffer;
declare module "ngx-tooltip";
declare module "papaparse";
declare module "file-saver";
declare module "url-parse";
declare module "ng-select";
